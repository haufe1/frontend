import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { StreamExperts, ToggleFilterFemaleExperts, ToggleFilterMaleExperts } from './api.actions';
import { ApiService } from './api.service';
import { Expert } from './expert.model';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

export interface ApiStateModel {
  experts: Expert[];
  showFemale: boolean;
  showMale: boolean;
  hasError: boolean;
}

@State<ApiStateModel>({
  name: 'api',
  defaults: {
    experts: [],
    showFemale: true,
    showMale: true,
    hasError: false
  }
})
@Injectable()
export class ApiState {
  private streamExpertsSubscription: Subscription = new Subscription();

  constructor(private apiService: ApiService) {}

  @Action(StreamExperts)
  doStreamExperts(ctx: StateContext<ApiStateModel>) {
    this.streamExpertsSubscription.unsubscribe();
    this.streamExpertsSubscription = this.apiService.streamExperts()
      .pipe(map(expert => [expert, ...ctx.getState().experts]))
      .subscribe(experts => ctx.setState({
        ...ctx.getState(),
        experts: experts,
        hasError: false
      }),
        () => ctx.setState({ ...ctx.getState(), hasError: true }));
  }

  @Action(ToggleFilterFemaleExperts)
  doToggleFilterFemaleExperts(ctx: StateContext<ApiStateModel>) {
    const state = ctx.setState({ ...ctx.getState(), showFemale: !ctx.getState().showFemale }) as any;
    this.sendFilterMessage(state.api);
  }

  @Action(ToggleFilterMaleExperts)
  doToggleFilterMaleExperts(ctx: StateContext<ApiStateModel>) {
    const state = ctx.setState({ ...ctx.getState(), showMale: !ctx.getState().showMale }) as any;
    this.sendFilterMessage(state.api);
  }

  @Selector()
  static experts(state: ApiStateModel) {
    return state.experts
      .filter(expert => (state.showFemale && expert.gender === 'female') || (state.showMale && expert.gender === 'male'));
  }

  private sendFilterMessage(state: ApiStateModel): void {
    this.apiService.send(state.showFemale === state.showMale ? '' : state.showFemale ? 'FEMALE' : 'MALE');
  }
}
