export class StreamExperts {
  static readonly type = '[API] Stream Experts';
  constructor() {}
}

export class ToggleFilterFemaleExperts {
  static readonly type = '[API] ToggleFilterFemaleExperts';
  constructor() {}
}

export class ToggleFilterMaleExperts {
  static readonly type = '[API] ToggleFilterMaleExperts';
  constructor() {}
}
