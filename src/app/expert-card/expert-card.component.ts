import { Component, Input, OnInit } from '@angular/core';
import { Expert } from '../expert.model';

@Component({
  selector: 'expert-card',
  templateUrl: './expert-card.component.html',
  styleUrls: ['./expert-card.component.scss']
})
export class ExpertCardComponent implements OnInit {
  private static readonly TEXT_LENGTH_SHORT = 140;

  @Input()
  expert!: Expert;
  hideDetails = true;

  get shortText(): string {
    return this.expert.text.length > ExpertCardComponent.TEXT_LENGTH_SHORT ?
      `${this.expert.text.substring(0, ExpertCardComponent.TEXT_LENGTH_SHORT)}...`
      : this.expert.text;
  }

  ngOnInit(): void {
  }

  toggleDetails(): void {
    this.hideDetails = !this.hideDetails;
  }
}
