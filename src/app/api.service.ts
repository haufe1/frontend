import { Injectable } from '@angular/core';
import { LoremIpsum } from 'lorem-ipsum';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { environment } from 'src/environments/environment';
import { Expert } from './expert.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private static readonly TEXT_LENGTH_MIN = 100;
  private static readonly TEXT_LENGTH_MAX = 300;
  
  private readonly lorem = new LoremIpsum({
    sentencesPerParagraph: { max: 8, min: 4 },
    wordsPerSentence: { max: 16, min: 4}
  });

  private webSocket: WebSocketSubject<any> = webSocket(environment.expertApi);

  public streamExperts(): Observable<Expert> {
    return this.webSocket
      .asObservable()
      .pipe(filter(Boolean),
        map(json => json as Expert),
        tap(expert => expert.text = this.generateRandomWords()));
  }

  public send(message: string): void {
    this.webSocket.next(message);
  }

  private generateRandomWords(): string {
    const wordCount = Math.floor(Math.random() * (ApiService.TEXT_LENGTH_MAX - ApiService.TEXT_LENGTH_MIN ) + ApiService.TEXT_LENGTH_MIN); 
    return this.lorem.generateWords(wordCount);
  }
}
