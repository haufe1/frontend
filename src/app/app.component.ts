import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { StreamExperts, ToggleFilterFemaleExperts, ToggleFilterMaleExperts } from './api.actions';
import { ApiState, ApiStateModel } from './api.state';
import { Expert } from './expert.model';

@Component({
  selector: 'exp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @Select(ApiState) state!: Observable<ApiStateModel>;
  @Select(ApiState.experts) experts!: Observable<Expert[]>;

  constructor(private store: Store) {}

  public ngOnInit() {
    this.store.dispatch(new StreamExperts()).subscribe();
  }

  toggleFilterFemale(): void {
    this.store.dispatch(new ToggleFilterFemaleExperts()).subscribe();
  }

  toggleFilterMale(): void {
    this.store.dispatch(new ToggleFilterMaleExperts()).subscribe();
  }
}
