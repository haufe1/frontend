export interface Expert {
  title: string;
  firstName: string;
  lastName: string;
  gender: string;
  photoUrl: string;
  text: string;
  address: {
    streetName: string;
    streetNumber: string;
    city: string;
    country: string;
    postcode: string;
  }
}
